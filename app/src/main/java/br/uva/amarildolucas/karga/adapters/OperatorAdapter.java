package br.uva.amarildolucas.karga.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.ImageView;
import br.uva.amarildolucas.karga.R;
import br.uva.amarildolucas.karga.models.Operator;
import java.util.ArrayList;
import android.view.LayoutInflater;

public class OperatorAdapter extends ArrayAdapter<Operator> {
    private ArrayList<Operator> operators;

    public OperatorAdapter(Context context, int textViewResourceId, ArrayList<Operator> operators) {
        super(context, textViewResourceId, operators);
        this.operators = operators;
    }

    @Override
    public int getCount() {
        return operators.size();
    }

    @Override
    public Operator getItem(int position) {
        Operator operator = operators.get(position);
        return operator;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.operator_layout_claro, null);
        }

        Operator operator = operators.get(position);

        if (operator != null) {
            TextView nameTextView = view.findViewById(R.id.operator_name);

            if (nameTextView != null) {
                nameTextView.setText(operator.getName());
            }
        }

        return view;
    }
}