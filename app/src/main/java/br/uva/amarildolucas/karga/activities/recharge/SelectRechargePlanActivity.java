package br.uva.amarildolucas.karga.activities.recharge;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.Serializable;
import java.util.ArrayList;

import br.uva.amarildolucas.karga.R;
import br.uva.amarildolucas.karga.adapters.PlanAdapter;
import br.uva.amarildolucas.karga.models.AppDatabase;
import br.uva.amarildolucas.karga.models.Operator;
import br.uva.amarildolucas.karga.models.OperatorDao;
import br.uva.amarildolucas.karga.models.Plan;
import br.uva.amarildolucas.karga.models.PlanDao;

public class SelectRechargePlanActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private ListView listViewPlan;
    private PlanAdapter planAdapter;
    private Context context = this;

    public ArrayList<Plan> plans = null;
    public ArrayList<Operator> operators = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_recharge_plan);

        getPlans();

        planAdapter = new PlanAdapter(context, R.layout.plan_layout_claro, plans);
        ListView listView = findViewById(R.id.listViewPlan);
        listView.setAdapter(planAdapter);
        listView.setOnItemClickListener(this);
    }

    /* Events */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Operator operator = (Operator) getIntent().getSerializableExtra("operator");
        Plan plan = plans.get(position);

        Intent intent = new Intent(context, FinalizeRechargePaymentActivity.class);
        intent.putExtra("plan", plan);
        intent.putExtra("operator", operator);
        intent.putExtra("phone", getIntent().getExtras().get("phone").toString());

        startActivity(intent);
    }

    /* Methods */
    private void getPlans(){
        AppDatabase mDb = Room.databaseBuilder(this, AppDatabase.class, "Karga").allowMainThreadQueries().build();

        PlanDao planDao = mDb.planDao();
        planDao.delteAll();

        OperatorDao mOperatorDao = mDb.operatorDao();
        operators = (ArrayList<Operator>) mOperatorDao.getAll();

        Plan plan = new Plan();
        plan.setPrice(15.00);
        plan.setId(1);
        plan.setValidityDays(30);
        plan.setOperator(operators.get(0));
        planDao.insertAll(plan);

        plans = (ArrayList<Plan>) planDao.getAll();
        mDb.close();

    }
}
