package br.uva.amarildolucas.karga.models;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverter;

import java.io.Serializable;

@Entity
public class Plan implements Serializable {
    @PrimaryKey
    private int id;
    private Double price;
    private Integer validityDays;
    @Ignore
    private Operator operator;

    public Double getPrice() { return price; }
    public void setPrice(Double price) { this.price = price; }

    public Integer getValidityDays() {
        return validityDays;
    }
    public void setValidityDays(Integer validityDays) { this.validityDays = validityDays; }

    public Operator getOperator() { return operator; }
    public void setOperator(Operator operator) { this.operator = operator; }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

