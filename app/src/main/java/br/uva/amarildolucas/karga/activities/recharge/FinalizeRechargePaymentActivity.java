package br.uva.amarildolucas.karga.activities.recharge;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import br.uva.amarildolucas.karga.R;
import br.uva.amarildolucas.karga.models.AppDatabase;
import br.uva.amarildolucas.karga.models.Operator;
import br.uva.amarildolucas.karga.models.Plan;
import br.uva.amarildolucas.karga.models.Recharge;

import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import java.io.Serializable;
import java.util.Random;

public class FinalizeRechargePaymentActivity extends AppCompatActivity implements OnClickListener {
    AppDatabase mDb;
    Recharge recharge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finalize_recharge_payment);
        mDb = Room.databaseBuilder(this, AppDatabase.class, "Karga").fallbackToDestructiveMigration().allowMainThreadQueries().build();

        Operator operator = (Operator) getIntent().getSerializableExtra("operator");
        TextView operatorNameTextView = findViewById(R.id.recharge_fullname);
        operatorNameTextView.setText(operator.getName());

        Plan plan = (Plan) getIntent().getSerializableExtra("plan");
        TextView planTextView = findViewById(R.id.recharge_value);
        planTextView.setText("R$ " + plan.getPrice().toString());

        String phone = getIntent().getStringExtra("phone");
        TextView phoneTextView = findViewById(R.id.recharge_phone_number);
        phoneTextView.setText(phone);

        recharge = new Recharge();
        recharge.setId(new Random().nextInt());
        recharge.setPhoneNumber(getIntent().getExtras().get("phone").toString());
        recharge.setPlan((Plan) getIntent().getExtras().getSerializable("plan"));
        recharge.setPromoCode("");


        Button buttonFinalizePayment = (Button) findViewById(R.id.buttonFinalizePayment);
        buttonFinalizePayment.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        mDb.rechargeDao().insertAll(recharge);
        mDb.close();
        Intent intent = new Intent(this, ListRecharges.class);
        startActivity(intent);
        finish();
    }
}
