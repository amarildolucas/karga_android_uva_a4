package br.uva.amarildolucas.karga.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

/**
 * Created by amarildolucas on 07/05/2018.
 */

@Entity

public class Operator implements Serializable {
    @PrimaryKey private int id;
    private String name;
    private String logo;
    private String  type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }
    public void setLogo(String logo) { this.logo = logo; }

    public String getType() {
        return type;
    }
    public void setType(String type) { this.type = type; }
}
