package br.uva.amarildolucas.karga.models;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface RechargeDao {
    @Query("Select * FROM recharge")
    List<Recharge> getAll();

    @Query("SELECT * FROM recharge WHERE id IN (:rechargeIds)")
    List<Recharge> loadAllByIds(int[] rechargeIds);

    @Query("SELECT * FROM recharge WHERE id = :id LIMIT 1")
    Operator findById(String id);

    @Insert
    void insertAll(Recharge... recharges);

    @Delete
    void delete(Recharge recharge);

    @Query("DELETE FROM recharge")
    void delteAll();
}
