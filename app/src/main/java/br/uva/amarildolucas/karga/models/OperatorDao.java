package br.uva.amarildolucas.karga.models;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import java.util.List;

@Dao
public interface OperatorDao {

    @Query("Select * FROM operator")
    List<Operator> getAll();

    @Query("SELECT * FROM operator WHERE id IN (:operatorIds)")
    List<Operator> loadAllByIds(int[] operatorIds);

    @Query("SELECT * FROM operator WHERE name LIKE :name LIMIT 1")
    Operator findByName(String name);

    @Insert
    void insertAll(Operator... operators);

    @Delete
    void delete(Operator operator);

    @Query("DELETE FROM operator")
    void delteAll();
}
