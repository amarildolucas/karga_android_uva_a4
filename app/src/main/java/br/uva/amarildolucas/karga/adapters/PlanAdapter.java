package br.uva.amarildolucas.karga.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.ImageView;
import br.uva.amarildolucas.karga.R;
import br.uva.amarildolucas.karga.models.Plan;
import java.util.ArrayList;
import android.view.LayoutInflater;

public class PlanAdapter extends ArrayAdapter<Plan> {
    private ArrayList<Plan> plans;

    public PlanAdapter(Context context, int textViewResourceId, ArrayList<Plan> plans) {
        super(context, textViewResourceId, plans);
        this.plans = plans;
    }

    @Override
    public int getCount() {
        return plans.size();
    }

    @Override
    public Plan getItem(int position) {
        Plan plan = plans.get(position);
        return plan;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.plan_layout_claro, null);
        }

        Plan plan = plans.get(position);

        if (plan != null) {
            TextView priceTextView = view.findViewById(R.id.plan_price);
            TextView validityDaysTextView = view.findViewById(R.id.plan_validity);

            if (priceTextView != null) {
                priceTextView.setText("R$ " + plan.getPrice().toString());
            }

            if (validityDaysTextView != null) {
                validityDaysTextView.setText(plan.getValidityDays().toString() + " DIAS");
            }
        }

        return view;
    }
}