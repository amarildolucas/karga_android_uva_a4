package br.uva.amarildolucas.karga.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import br.uva.amarildolucas.karga.R;
import br.uva.amarildolucas.karga.models.Recharge;

public class RechargeAdapter extends ArrayAdapter<Recharge> {
    private ArrayList<Recharge> recharges;

    public RechargeAdapter(Context context, int textViewResourceId, ArrayList<Recharge> recharges) {
        super(context, textViewResourceId, recharges);
        this.recharges = recharges;
    }

    @Override
    public int getCount() {
        return recharges.size();
    }

    @Override
    public Recharge getItem(int position) {
        Recharge recharge = recharges.get(position);
        return recharge;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.recharge_item_layout, null);
        }

        Recharge recharge = recharges.get(position);

        if (recharge != null) {
            TextView operatorNameTextView = view.findViewById(R.id.recharge_fullname);
            TextView phoneTextView = view.findViewById(R.id.recharge_phone_number);
            TextView priceTextView = view.findViewById(R.id.recharge_value);

            /*if (operatorNameTextView != null) {
                operatorNameTextView.setText(recharge.getPlan().getOperator().getName());
            }*/

            if (phoneTextView != null) {
                phoneTextView.setText(recharge.getPhoneNumber());
            }

            /*if (priceTextView != null) {
                priceTextView.setText(recharge.getPlan().getPrice().toString());
            }*/
        }

        return view;
    }
}
