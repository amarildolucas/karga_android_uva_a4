package br.uva.amarildolucas.karga.activities.recharge;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.support.annotation.WorkerThread;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import br.uva.amarildolucas.karga.R;
import br.uva.amarildolucas.karga.models.AppDatabase;
import br.uva.amarildolucas.karga.models.Operator;
import br.uva.amarildolucas.karga.models.OperatorDao;

public class SelectRechargePhoneNumberActivity extends AppCompatActivity implements OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_recharge_phone_number);
        createDatabaseEntities();
        Button buttonConfirmPhoneToRecharge = (Button) findViewById(R.id.buttonConfirmPhoneToRecharge);
        buttonConfirmPhoneToRecharge.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        TextInputEditText textViewPhoneToRecharge =  findViewById(R.id.textViewPhoneToRecharge);
        String phoneNumber = textViewPhoneToRecharge.getText().toString();

        Intent intent = new Intent(this, SelectRechargeOperatorActivity.class);
        intent.putExtra("phone", phoneNumber);
        startActivity(intent);
    }

    @WorkerThread
    private void createDatabaseEntities() {
        AppDatabase mDb = Room.databaseBuilder(this, AppDatabase.class, "Karga").fallbackToDestructiveMigration().allowMainThreadQueries().build();
        OperatorDao mOperatorDao = mDb.operatorDao();
        mOperatorDao.delteAll();

        Operator operator = new Operator();
        operator.setId(10);
        operator.setName("CLARO");
        operator.setLogo(null);
        operator.setType("");

        Operator operator1 = new Operator();
        operator1.setId(3);
        operator1.setName("TIM");
        operator1.setLogo(null);
        operator1.setType("");

        mOperatorDao.insertAll(operator, operator1);
        mOperatorDao.getAll();
    }
}
