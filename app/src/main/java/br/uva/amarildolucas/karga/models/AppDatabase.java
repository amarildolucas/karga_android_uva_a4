package br.uva.amarildolucas.karga.models;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Operator.class, Plan.class, Recharge.class}, version = 3, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract OperatorDao operatorDao();
    public abstract PlanDao planDao();
    public abstract RechargeDao rechargeDao();
}
