package br.uva.amarildolucas.karga.activities.recharge;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import br.uva.amarildolucas.karga.R;
import br.uva.amarildolucas.karga.adapters.RechargeAdapter;
import br.uva.amarildolucas.karga.models.AppDatabase;
import br.uva.amarildolucas.karga.models.Recharge;
import br.uva.amarildolucas.karga.models.RechargeDao;

public class ListRecharges extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private RechargeAdapter rechargeAdapter;
    private Context context = this;

    public ArrayList<Recharge> recharges = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_recharges);

        getRecharges();

        rechargeAdapter = new RechargeAdapter(context, R.layout.recharge_item_layout, recharges);
        ListView listView = findViewById(R.id.listRechargesLayout);
        listView.setAdapter(rechargeAdapter);
        listView.setOnItemClickListener(this);
    }

    private void getRecharges() {
        AppDatabase mDb = Room.databaseBuilder(this, AppDatabase.class, "Karga").fallbackToDestructiveMigration().allowMainThreadQueries().build();
        RechargeDao mRechargeDao = mDb.rechargeDao();
        recharges = (ArrayList<Recharge>) mRechargeDao.getAll();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {}
}
