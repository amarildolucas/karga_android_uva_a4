package br.uva.amarildolucas.karga.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class Recharge implements Serializable {
    @PrimaryKey
    private int id;
    @Ignore
    private Plan plan;
    private String phoneNumber;
    private String promoCode;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public Plan getPlan() { return plan; }
    public void setPlan(Plan plan) { this.plan = plan; }

    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) { this.phoneNumber = phoneNumber; }

    public String getPromoCode() { return promoCode; }
    public void setPromoCode(String promoCode) { this.promoCode = promoCode; }

    @Override
    public String toString() {
        return phoneNumber;
    }
}