package br.uva.amarildolucas.karga.models;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface PlanDao {

        @Query("Select * FROM `plan`")
        List<Plan> getAll();

        @Query("SELECT * FROM `plan` WHERE id IN (:planIds)")
        List<Plan> loadAllByIds(int[] planIds);

        @Insert
        void insertAll(Plan... plans);

        @Delete
        void delete(Plan plan);

        @Query("DELETE FROM `plan`")
        void delteAll();

}
