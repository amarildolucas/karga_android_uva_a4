package br.uva.amarildolucas.karga;

import android.arch.persistence.room.Room;
import android.support.annotation.WorkerThread;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import br.uva.amarildolucas.karga.models.AppDatabase;
import br.uva.amarildolucas.karga.models.Operator;
import br.uva.amarildolucas.karga.models.OperatorDao;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createDatabaseEntities();

    }

    @WorkerThread
    private void createDatabaseEntities() {
        AppDatabase mDb = Room.inMemoryDatabaseBuilder(this, AppDatabase.class).build();
        OperatorDao mOperatorDao = mDb.operatorDao();
        Operator operator = new Operator();
        operator.setName("Claro");
        operator.setLogo(null);
        operator.setType("ok");

        Operator operator1 = new Operator();
        operator1.setName("TIM");
        operator1.setLogo(null);
        operator1.setType("sas");
        mOperatorDao.insertAll(operator, operator1);
    }
}
