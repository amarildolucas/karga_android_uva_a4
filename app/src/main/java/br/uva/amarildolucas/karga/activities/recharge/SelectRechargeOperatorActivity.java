package br.uva.amarildolucas.karga.activities.recharge;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ListView;
import br.uva.amarildolucas.karga.R;
import br.uva.amarildolucas.karga.adapters.OperatorAdapter;
import br.uva.amarildolucas.karga.models.AppDatabase;
import br.uva.amarildolucas.karga.models.Operator;
import br.uva.amarildolucas.karga.models.OperatorDao;

import java.io.Serializable;
import java.util.ArrayList;

import android.view.View;

public class SelectRechargeOperatorActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private OperatorAdapter operatorAdapter;
    private Context context = this;

    public ArrayList<Operator> operators = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_recharge_operator);

        getOperators();

        operatorAdapter = new OperatorAdapter(context, R.layout.operator_layout_claro, operators);
        ListView listView = findViewById(R.id.listViewOperator);
        listView.setAdapter(operatorAdapter);
        listView.setOnItemClickListener(this);
    }

    /* Events */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Operator operator = operators.get(position);
        Intent intent = new Intent(context, SelectRechargePlanActivity.class);
        intent.putExtra("operator", operator);
        intent.putExtra("phone", getIntent().getExtras().get("phone").toString());
        startActivity(intent);
    }

    private void getOperators() {
        AppDatabase mDb = Room.databaseBuilder(this, AppDatabase.class, "Karga").fallbackToDestructiveMigration().allowMainThreadQueries().build();
        OperatorDao mOperatorDao = mDb.operatorDao();
        operators = (ArrayList<Operator>) mOperatorDao.getAll();
    }
}
